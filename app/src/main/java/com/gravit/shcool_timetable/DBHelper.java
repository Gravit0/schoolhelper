package com.gravit.shcool_timetable;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.gravit.shcool_timetable.generalVars;
/**
 * Created by gravit on 22.07.16.
 */
public class DBHelper  extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "maindb";
    public static final String TABLE_CONFIG = "config";
    public static final String TABLE_LESSOINS = "lessons";
    public static final String TABLE_timetable = "timetable";
    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE `config` (\n" +
                "  `id` bigint(20) NOT NULL,\n" +
                "  `m_key` text NOT NULL,\n" +
                "  `m_value` text NOT NULL\n" +
                ")");
        db.execSQL("CREATE TABLE `lessons` (\n" +
                "  `id` bigint(20) NOT NULL,\n" +
                "  `weekday` tinyint(4) NULL,\n" +
                "  `predmet` text NOT NULL,\n" +
                "  `day` date NULL,\n" +
                "  `tb_type` int(11) NOT NULL\n" +
                ")\n");
        db.execSQL("CREATE TABLE `timetable` (\n" +
                "  `id` bigint(20) NOT NULL,\n" +
                "  `value` varchar(32) NOT NULL,\n" +
                "  `weekdays` varchar(128) NOT NULL,\n" +
                "  `serverid` bigint(20) NOT NULL\n" +
                ")");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE_CONFIG);
        db.execSQL("drop table if exists " + TABLE_LESSOINS);
        db.execSQL("drop table if exists " + TABLE_timetable);
        onCreate(db);

    }
}