package com.gravit.shcool_timetable;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(generalVars.active==false)
        {
            generalVars.init(this);
            
        }
        SQLiteDatabase sqldata = generalVars.database.getReadableDatabase();
        Cursor cursor = sqldata.rawQuery("SELECT * FROM timetable", new String[] {});
        int arach = cursor.getCount();
        Log.v("f",String.valueOf(arach));
    }
}
